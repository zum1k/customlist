package com.andersenlab.trainee.customlist;

import org.junit.Assert;
import org.junit.Test;

public class CustomListImplTest {
  @Test
  public void testAdd_ShouldReturn_True() {
    CustomList<String> list = new CustomListImpl<>();
    String expectedElement = "expected";

    Assert.assertTrue(list.add(expectedElement));
  }

  @Test
  public void testGet_ShouldReturn_Object() {
    CustomList<String> list = new CustomListImpl<>();
    String expectedElement = "expected";
    list.add(expectedElement);

    String actualElement = list.get(0);

    Assert.assertEquals(expectedElement, actualElement);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testGet_NegativeNumber_ShouldReturn_Exception() throws IndexOutOfBoundsException {
    CustomList<String> list = new CustomListImpl<>();
    String expectedElement = "expected";
    list.add(expectedElement);

    list.get(-1);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testGet_IndexLargerThanSizeOfList_ShouldReturn_Exception() throws IndexOutOfBoundsException {
    CustomList<String> list = new CustomListImpl<>();
    String expectedElement = "expected";
    list.add(expectedElement);

    list.get(30);
  }

  @Test
  public void testRemove_ShouldReturn_True() {
    CustomList<String> list = new CustomListImpl<>();
    String firstElement = "expected1";
    String secondElement = "expected2";
    list.add(firstElement);
    list.add(secondElement);

    Assert.assertTrue(list.remove(firstElement));
  }

  @Test
  public void testRemove_ShouldReturn_False() {
    CustomList<String> list = new CustomListImpl<>();
    String firstElement = "expected1";
    String secondElement = "expected2";
    String unexpectedElement = "expected3";
    list.add(firstElement);
    list.add(secondElement);

    Assert.assertFalse(list.remove(unexpectedElement));
  }

  @Test
  public void testIsEmpty_ShouldReturn_True() {
    CustomList<String> list = new CustomListImpl<>();

    Assert.assertTrue(list.isEmpty());
  }

  @Test
  public void testIsEmpty_ShouldReturn_False() {
    CustomList<String> list = new CustomListImpl<>();
    String firstElement = "expected1";
    list.add(firstElement);

    Assert.assertFalse(list.isEmpty());
  }

  @Test
  public void testSize_ShouldReturn_Number() {
    CustomList<String> list = new CustomListImpl<>();
    String firstElement = "expected1";
    list.add(firstElement);

    int expectedSize = 1;
    int actualSize = list.size();

    Assert.assertEquals(expectedSize, actualSize);
  }

  @Test
  public void testSize_ShouldReturn_Zero() {
    CustomList<String> list = new CustomListImpl<>();
    int expectedSize = 0;
    int actualSize = list.size();

    Assert.assertEquals(expectedSize, actualSize);
  }

  @Test
  public void testContains_ShouldReturn_True() {
    CustomList<String> list = new CustomListImpl<>();
    String expectedElement = "expected1";
    list.add(expectedElement);

    Assert.assertTrue(list.contains(expectedElement));
  }

  @Test
  public void testContains_ShouldReturn_False() {
    CustomList<String> list = new CustomListImpl<>();
    String firstElement = "expected1";
    list.add(firstElement);

    String expectedElement = "expected2";

    Assert.assertFalse(list.contains(expectedElement));
  }

  @Test
  public void testSet_ShouldReturn_Entity() {
    CustomList<String> list = new CustomListImpl<>();
    String firstElement = "expected1";
    list.add(firstElement);

    String expectedElement = "expected2";
    int expectedIndex = 0;

    String actualElement = list.set(expectedIndex, expectedElement);

    Assert.assertEquals(expectedElement, actualElement);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testSet_NegativeNumber_ShouldReturn_Exception() {
    CustomList<String> list = new CustomListImpl<>();
    String firstElement = "expected1";
    list.add(firstElement);

    String expectedElement = "expected2";
    int expectedIndex = 1;

    list.set(expectedIndex, expectedElement);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testSet_NumberLargerThanListSize_ShouldReturn_Exception() {
    CustomList<String> list = new CustomListImpl<>();
    String firstElement = "expected1";
    list.add(firstElement);

    String expectedElement = "expected2";
    int expectedIndex = -10;

    list.set(expectedIndex, expectedElement);
  }

  @Test
  public void testIndexOf_ShouldReturn_Number() {
    CustomList<String> list = new CustomListImpl<>();
    String expectedElement = "expected1";
    int expectedIndex = 0;
    list.add(expectedElement);

    int actualIndex = list.indexOf(expectedElement);

    Assert.assertEquals(expectedIndex, actualIndex);
  }

  @Test
  public void testIndexOf_ShouldReturn_Negative() {
    CustomList<String> list = new CustomListImpl<>();
    String firstElement = "expected1";
    list.add(firstElement);
    int expectedIndex = -1;
    String expectedElement = "expected2";

    int actualIndex = list.indexOf(expectedElement);

    Assert.assertEquals(expectedIndex, actualIndex);
  }

  @Test
  public void testClear_ShouldReturn_True() {
    CustomList<String> list = new CustomListImpl<>();
    String firstElement = "expected1";
    String secondElement = "expected2";
    list.add(firstElement);
    list.add(secondElement);
    int expectedSize = 0;

    list.clear();
    int actualSize = list.size();

    Assert.assertEquals(expectedSize, actualSize);
  }
}