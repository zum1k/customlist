package com.andersenlab.trainee.customlist;

/**
 * This interface implements the List interface and it's methods
 *
 * @param <T> the type of objects with which implementation operates.
 */

public interface CustomList<T> {

  /**
   * Adds object T to collection
   *
   * @param t T
   * @return boolean
   */
  boolean add(T t);

  /**
   * Get object from collect by it's index
   *
   * @param index int
   * @return Object t
   * @throws IndexOutOfBoundsException if index is negative number or larger than size of list
   */
  T get(int index);

  /**
   * Remove Object from collection
   *
   * @param o Object
   * @return boolean, true if Object contains in list, false if not
   */
  boolean remove(Object o);

  /**
   * Checks if the list is empty
   * *
   *
   * @return boolean, true - if list is empty, false if not
   */
  boolean isEmpty();

  /**
   * Returns the number of stored objects
   *
   * @return number of stored objects
   */
  int size();

  /**
   * Checks the list for the presence of an object
   *
   * @param o Object
   * @return boolean, true if Object contains in list, false if not
   */
  boolean contains(Object o);

  /**
   * Overwrites an object with a new object at the index
   *
   * @param index(int) of the rewritable object in the list
   * @param t(T)       - new object
   * @return Object t
   * @throws IndexOutOfBoundsException if index is negative number or larger than size of list
   */
  T set(int index, T t);

  /**
   * Return index of object
   *
   * @param t(T) checked object
   * @return index of stored object if found, -1 if not
   */
  int indexOf(T t);

  /**
   * Clear list from stored objects
   */
  void clear();
}
