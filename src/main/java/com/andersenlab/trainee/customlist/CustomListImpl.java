package com.andersenlab.trainee.customlist;

public class CustomListImpl<T> implements CustomList<T> {
  private T[] arr;
  private int listSize = 0;
  private static final int DEFAULT_SIZE = 16;
  private static final Object[] EMPTY_ELEMENTDATA = {};

  public CustomListImpl(T[] arr) {
    this.arr = arr;
    this.listSize = arr.length;
  }

  public CustomListImpl(int size) {
    if (size > 0) {
      this.arr = (T[]) new Object[size];
    } else if (size == 0) {
      this.arr = (T[]) EMPTY_ELEMENTDATA;
    } else {
      throw new IllegalArgumentException("Illegal Capacity: " +
          size);
    }
  }

  public CustomListImpl() {
    arr = (T[]) new Object[DEFAULT_SIZE];
  }

  @Override
  public boolean add(T t) {
    if (listSize == arr.length) {
      int newSize = arr.length + arr.length * 5 / 10;
      T[] newList = (T[]) new Object[newSize];
      System.arraycopy(arr, 0, newList, 0, arr.length);
      newList[listSize] = t;
      int bufferSize = listSize++;
      clear();
      this.arr = (T[]) new Object[newSize];
      System.arraycopy(newList, 0, arr, 0, arr.length);
      this.listSize = bufferSize;
      return true;
    }
    arr[listSize] = t;
    this.listSize++;
    return true;
  }

  @Override
  public T get(int index) {
    if (index >= arr.length || index < 0) {
      throw new IndexOutOfBoundsException(index);
    }
    return (T) arr[index];
  }

  @Override
  public boolean remove(Object o) {
    int objectIndex = isContains(o);
    if (objectIndex == -1) {
      return false;
    }
    for (int i = objectIndex; i < listSize; i++) {
      arr[i] = arr[i + 1];
    }
    arr[listSize] = null;
    listSize--;
    return true;
  }

  @Override
  public boolean isEmpty() {
    return this.listSize == 0;
  }

  @Override
  public int size() {
    return this.listSize;
  }

  @Override
  public boolean contains(Object o) {
    for (int i = 0; i < listSize; i++) {
      if (arr[i].equals(o)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public T set(int index, T t) {
    if (index >= listSize || index < 0) {
      throw new IndexOutOfBoundsException("Index out of bounds " + index);
    }
    arr[index] = t;
    return t;
  }

  @Override
  public int indexOf(T t) {
    for (int i = 0; i < arr.length; i++) {
      if (t.equals(arr[i])) {
        return i;
      }
    }
    return -1;
  }

  @Override
  public void clear() {
    for (int i = 0; i < listSize; i++) {
      arr[i] = null;
    }
    listSize = 0;
  }

  private int isContains(Object o) {
    int objectIndex = -1;
    for (int i = 0; i < listSize; i++) {
      if (arr[i] == o) {
        objectIndex = i;
      }
    }
    return objectIndex;
  }
}
